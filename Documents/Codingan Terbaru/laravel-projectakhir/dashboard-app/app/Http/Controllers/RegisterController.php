<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Session;


class RegisterController extends Controller
{
    public function register()
    {
        return view('/register');
    }

    public function actionregister(Request $request)
    {
        $user = new User;
        $user -> name = $request->input('name');
        $user -> email = $request->input('email');
        $user -> password = Hash::make($request->input('password'));

        $user->save();
        return redirect('/login')->with('success', 'regis berhasil');
    }
}
    